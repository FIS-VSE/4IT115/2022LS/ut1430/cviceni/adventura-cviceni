package cz.vse.xname.adventura1430.main;

import cz.vse.xname.adventura1430.logika.*;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.util.Collection;
import java.util.HashMap;

public class HomeController implements Pozorovatel {

    @FXML private ImageView hrac;
    @FXML private ListView<Prostor> panelVychodu;
    @FXML private Button odesli;
    @FXML private TextArea vystup;
    @FXML private TextField vstup;

    private IHra hra = new Hra();
    private HashMap<String, Point2D> souradniceProstoru;
    private HashMap<String, ImageView> obrazkyProstoru;


    @FXML private void initialize() {
        hra.getHerniPlan().registruj(this);
        vystup.appendText(hra.vratUvitani()+"\n\n");
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                vstup.requestFocus();
            }
        });
        aktualizujPanelVychodu();
        vytvorSouradniceProstoru();
        aktualizujPolohuHrace();
        vytvorObrazkyProstoru();
        panelVychodu.setCellFactory(listView -> new ProstorCell());
    }

    private ImageView getObrazekProstoru(Prostor prostor) {
        if(obrazkyProstoru.containsKey(prostor.getNazev())) {
            return obrazkyProstoru.get(prostor.getNazev());
        } else return obrazkyProstoru.get(null);
    }

    private void vytvorSouradniceProstoru() {
        souradniceProstoru = new HashMap<>();
        souradniceProstoru.put("domeček", new Point2D(14,71));
        souradniceProstoru.put("les", new Point2D(89, 36));
        souradniceProstoru.put("hluboký_les", new Point2D(148, 80));
        souradniceProstoru.put("jeskyně", new Point2D(154, 154));
        souradniceProstoru.put("chaloupka", new Point2D(226, 36));
    }

    private void vytvorObrazkyProstoru() {
        obrazkyProstoru = new HashMap<>();
        obrazkyProstoru.put("les", vratObrazekProstoru("les.jpg"));
        obrazkyProstoru.put(null, vratObrazekProstoru("prostor.jpg"));
    }

    private ImageView vratObrazekProstoru(String souborObrazku) {
        String obrazkyProstoru = getClass().getResource("/obrazkyProstoru").toExternalForm();
        ImageView obrazek = new ImageView(obrazkyProstoru+souborObrazku);
        obrazek.setFitHeight(70);
        obrazek.setPreserveRatio(true);
        return obrazek;
    }

    private void aktualizujPolohuHrace() {
        Prostor prostor = hra.getHerniPlan().getAktualniProstor();
        Point2D souradnice = souradniceProstoru.get(prostor.getNazev());
        hrac.setLayoutX(souradnice.getX());
        hrac.setLayoutY(souradnice.getY());
    }

    private void aktualizujPanelVychodu() {
        panelVychodu.getItems().clear();
        Collection<Prostor> vychody = hra.getHerniPlan().getAktualniProstor().getVychody();
        panelVychodu.getItems().addAll(vychody);
    }

    public void odesliVstup(ActionEvent actionEvent) {
        String prikaz = vstup.getText();
        vstup.clear();
        zpracujPrikaz(prikaz);
    }

    private void zpracujPrikaz(String prikaz) {
        vystup.appendText("> "+prikaz+"\n\n");
        String vysledek = hra.zpracujPrikaz(prikaz);
        vystup.appendText(vysledek+"\n\n");

        if(hra.konecHry()) {
            vystup.appendText(hra.vratEpilog());
            vstup.setDisable(true);
            odesli.setDisable(true);
            panelVychodu.setDisable(true);
        }
    }

    @Override
    public void aktualizuj(PredmetPozorovani predmetPozorovani) {
        aktualizujPanelVychodu();
        aktualizujPolohuHrace();
    }

    @FXML
    private void klikPanelVychodu(MouseEvent mouseEvent) {
        Prostor novyProstor = panelVychodu.getSelectionModel().getSelectedItem();
        if(novyProstor == null) return;
        String prikaz = PrikazJdi.NAZEV +" "+novyProstor;
        zpracujPrikaz(prikaz);
    }

    public void klikHrac(MouseEvent mouseEvent) {
        zpracujPrikaz(PrikazObsahBatohu.NAZEV);
    }

    public void zobrazNapovedu(ActionEvent actionEvent) {
        Stage stage = new Stage();
        WebView ww = new WebView();
        stage.setScene(new Scene(ww));
        ww.getEngine().load(getClass().getResource("napoveda.html").toExternalForm());
        stage.show();
    }

    class ProstorCell extends ListCell<Prostor> {
        @Override
        protected void updateItem(Prostor prostor, boolean empty) {
            super.updateItem(prostor, empty);
            if(!empty) {
                setText(prostor.getNazev());
                setGraphic(getObrazekProstoru(prostor));
            } else {
                setText(null);
                setGraphic(null);
            }
        }
    }
}
