module cz.vse.xname.adventura1430 {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.web;

    exports cz.vse.xname.adventura1430.main;
    opens cz.vse.xname.adventura1430.main to javafx.fxml;
}